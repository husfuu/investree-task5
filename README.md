## Create Database

docker run --rm \
--name vix-investree-springboot-db \
-e POSTGRES_DB=vix_investree_springboot_db \
-e POSTGRES_USER=vix_investree_springboot \
-e POSTGRES_PASSWORD=mysecretpassword \
-e PGDATA=/var/lib/postgresql/data/pgdata \
-v "$PWD/vix-investree-springboot-db-data:/var/lib/postgresql/data" \
-p 5432:5432 \
postgres:13

psql -h 127.0.0.1 -U vix_investree_springboot vix_investree_springboot_db
