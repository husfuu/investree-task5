package com.investree.demo.investreetask5.controller;

import com.investree.demo.investreetask5.dto.TransactionUpdateStatus;
import com.investree.demo.investreetask5.dto.request.CreateTransactionRequest;
import com.investree.demo.investreetask5.dto.TransactionSave;
import com.investree.demo.investreetask5.dto.request.UpdateStatusTransactionRequest;
import com.investree.demo.investreetask5.model.TransactionStatus;
import com.investree.demo.investreetask5.view.impl.TransactionPaymentImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.jaxb.SpringDataJaxb.PageRequestDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1")
public class TransactionController {

    @Autowired
    private TransactionPaymentImpl transactionPayment;

    @PostMapping(value = "/transactions")
    public ResponseEntity<?> createTransaction(
            @RequestBody CreateTransactionRequest createTransactionRequest) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "create transaction success!");

        try {
            TransactionSave transaction = new TransactionSave();
            transaction.setId_meminjam(createTransactionRequest.getId_meminjam());
            transaction.setId_peminjam(createTransactionRequest.getId_peminjam());
            transaction.setTotal_loan(createTransactionRequest.getTotal_loan());
            transaction.setTenor(createTransactionRequest.getTenor());
            transaction.setPercent_interest(createTransactionRequest.getPercent_interest());
            transactionPayment.save(transaction);
            return new ResponseEntity<>(
                    resp,
                    HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "fail to create transaction : " + e.getMessage());
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/transactions/{transactionId}")
    public ResponseEntity<?> updateTransactionStatus(
            @PathVariable("transactionId") long transactionId,
            @RequestBody UpdateStatusTransactionRequest updateStatusTransactionRequest) {

        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "update transaction success!");

        try {
            TransactionUpdateStatus transactionUpdateStatus = new TransactionUpdateStatus();
            transactionUpdateStatus.setTransaction_id(transactionId);
            transactionUpdateStatus.setTransactionStatus(updateStatusTransactionRequest.getStatus());
            transactionPayment.updateStatus(transactionUpdateStatus);
            return new ResponseEntity<>(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "fail to update transaction : " + e.getMessage());
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // @GetMapping("/transactions/list/{status}/{size}/{page}")
    @GetMapping("/transactions/list")
    public ResponseEntity<?> getAllTransactionsByStatus(
            @RequestParam TransactionStatus status,
            @RequestParam int size,
            @RequestParam int page) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "get all transaction by status is success!");

        try {
            Pageable pageable = PageRequest.of(page, size);

            resp.put("data", transactionPayment.getAllTransactionByStatus(status, pageable));
            return new ResponseEntity<>(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "fail to get all transaction by status: " + e.getMessage());
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
