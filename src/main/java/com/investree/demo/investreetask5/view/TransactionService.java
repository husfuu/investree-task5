package com.investree.demo.investreetask5.view;

import org.springframework.data.domain.Pageable;

import com.investree.demo.investreetask5.dto.TransactionSave;
import com.investree.demo.investreetask5.dto.TransactionUpdateStatus;
import com.investree.demo.investreetask5.exception.NotFoundException;
import com.investree.demo.investreetask5.model.TransactionStatus;

public interface TransactionService {
    void save(TransactionSave transaction) throws NotFoundException;

    void updateStatus(TransactionUpdateStatus transactionUpdateStatus) throws NotFoundException;

    Iterable getAllTransactionByStatus(TransactionStatus status, Pageable pageable);
}
