package com.investree.demo.investreetask5.view.impl;

import com.investree.demo.investreetask5.dto.TransactionSave;
import com.investree.demo.investreetask5.dto.TransactionUpdateStatus;
import com.investree.demo.investreetask5.exception.NotFoundException;
import com.investree.demo.investreetask5.model.Transaction;
import com.investree.demo.investreetask5.model.TransactionStatus;
import com.investree.demo.investreetask5.model.Users;
import com.investree.demo.investreetask5.repository.TransactionRepository;
import com.investree.demo.investreetask5.repository.UsersRepository;
import com.investree.demo.investreetask5.view.TransactionService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TransactionPaymentImpl implements TransactionService {
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    UsersRepository usersRepository;

    @Override
    public void save(TransactionSave transactionSave) throws NotFoundException {
        Transaction newTransaction = new Transaction();

        Users peminjam_user = usersRepository.findById(transactionSave.getId_peminjam())
                .orElseThrow(() -> new NotFoundException("peminjam user is not found"));

        Users meminjam_user = usersRepository.findById(transactionSave.getId_meminjam())
                .orElseThrow(() -> new NotFoundException("meminjam user is not found"));

        newTransaction.setPeminjam(peminjam_user);
        newTransaction.setMeminjam(meminjam_user);
        newTransaction.setTotal_loan(transactionSave.getTotal_loan());
        newTransaction.setPercent_interest(transactionSave.getPercent_interest());
        newTransaction.setTenor(transactionSave.getTenor());
        transactionRepository.save(newTransaction);
    }

    @Override
    public void updateStatus(TransactionUpdateStatus transactionUpdateStatus) throws NotFoundException {
        Transaction updatedTransaction = transactionRepository.findById(transactionUpdateStatus.getTransaction_id())
                .orElseThrow(() -> new NotFoundException("transaction is not found"));

        System.out.println(transactionUpdateStatus.getTransactionStatus());
        updatedTransaction.setStatus(transactionUpdateStatus.getTransactionStatus());
    }

    @Override
    public Iterable getAllTransactionByStatus(TransactionStatus status, Pageable pageable) {
        return transactionRepository.findAllByStatus(status, pageable);
    }
}
