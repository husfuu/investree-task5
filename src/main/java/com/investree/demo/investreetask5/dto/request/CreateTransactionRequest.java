package com.investree.demo.investreetask5.dto.request;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateTransactionRequest {
    @NotNull
    private Long id_meminjam;
    @NotNull
    private Long id_peminjam;
    @NotNull
    private int tenor;
    @NotNull
    private Double total_loan;
    @NotNull
    private Double percent_interest;
}
