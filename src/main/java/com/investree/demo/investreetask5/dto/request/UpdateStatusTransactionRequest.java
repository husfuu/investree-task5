package com.investree.demo.investreetask5.dto.request;

import com.investree.demo.investreetask5.model.TransactionStatus;
import lombok.Data;

@Data
public class UpdateStatusTransactionRequest {
    private TransactionStatus status;
}
