package com.investree.demo.investreetask5.dto;

import lombok.Data;

@Data
public class PaymentSaveRequest {
    private Long transaction_id;
    private Double amount;
    private String payment_proof;
}
