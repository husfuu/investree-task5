package com.investree.demo.investreetask5.dto;

import lombok.Data;

@Data
public class TransactionSave {
    private Long id_meminjam;
    private Long id_peminjam;
    private int tenor;
    private Double total_loan;
    private Double percent_interest;
}
