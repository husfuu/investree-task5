package com.investree.demo.investreetask5.dto;

import com.investree.demo.investreetask5.model.TransactionStatus;
import lombok.Data;

@Data
public class TransactionUpdateStatus {
    private Long transaction_id;
    private TransactionStatus transactionStatus;
}
